
#version 330 core

uniform sampler2D diffuse_map;
uniform sampler2D second_texture;
in vec2 frag_tex_coords;

out vec4 out_color;

void main() {
    vec4 texel1 = texture(diffuse_map, frag_tex_coords);
    vec4 texel2 = texture(second_texture, frag_tex_coords);
    // Blend the two textures (you can use any blending function here)
    out_color = mix(texel1, texel2, 0.3);
}
