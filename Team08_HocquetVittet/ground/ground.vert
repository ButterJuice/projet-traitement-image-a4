#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

in vec3 position;
in vec2 tex_coord;
in vec2 tex_coord2;

out vec2 frag_tex_coords;
out vec2 frag_tex_coords2;

in vec3 normal;
out vec3 w_position, w_normal;

void main() {
    vec4 w_position4 = model * vec4(position, 1.0);
    gl_Position = projection * view * w_position4;

    frag_tex_coords = tex_coord;
    frag_tex_coords2 = tex_coord2;

    w_position = w_position4.xyz / w_position4.w;
    
    mat3 nit_matrix = transpose(inverse(mat3(model)));
    w_normal = normalize(nit_matrix * normal);
}
