
#version 330 core
in vec3 position;
uniform mat4 projectionNoZoom;
uniform mat4 projectionNoZoomMatrix;

in vec2 tex_coord;
out vec3 frag_tex_coords;


void main() {

    frag_tex_coords = position;
    gl_Position =  projectionNoZoom  * projectionNoZoomMatrix  * vec4(position, 1.0);


    
}
