#version 330 core

// input variables from vertex shader
in vec3 frag_position; // fragment position in world coordinates
in vec3 frag_normal;   // fragment normal in world coordinates
in vec2 frag_tex_coords; // texture coordinates for fragment

// output variable for fragment color
out vec4 out_color;

// uniforms for lighting and material properties
uniform vec3 light_dir;
uniform vec3 k_d;
uniform vec3 k_a;
uniform vec3 k_s;
uniform float s;
uniform vec3 w_camera_position;
uniform sampler2D diffuse_map;

void main() {
    // compute Phong illumination
    vec3 n = normalize(frag_normal);
    vec3 l = normalize(-light_dir);
    vec3 r = reflect(-l, n);
    vec3 v = normalize(w_camera_position - frag_position);

    vec3 diffuse_color = k_d * max(dot(n, l), 0);
    vec3 specular_color = k_s * pow(max(dot(r, v), 0), s);
    vec3 phong_color = k_a + diffuse_color + specular_color;

    // sample texture
    vec4 texture_color = texture(diffuse_map, frag_tex_coords);

    // combine Phong illumination and texture
    vec3 final_color = phong_color * texture_color.rgb;

    // output final color
    out_color = vec4(final_color, texture_color.a);
}
