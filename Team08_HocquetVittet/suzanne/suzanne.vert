#version 330 core

// input attribute variables
in vec3 position;
in vec3 normal;
in vec2 tex_coord;

// output variables for fragment shader
out vec3 frag_position; // fragment position in world coordinates
out vec3 frag_normal;   // fragment normal in world coordinates
out vec2 frag_tex_coords; // texture coordinates for fragment

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
    // calculate fragment position in world coordinates
    vec4 w_position4 = model * vec4(position, 1.0);
    gl_Position = projection * view * w_position4;
    frag_position = w_position4.xyz / w_position4.w; // dehomogenize

    // calculate fragment normal in world coordinates
    mat3 nit_matrix = transpose(inverse(mat3(model)));
    frag_normal = normalize(nit_matrix * normal);

    // pass texture coordinates to fragment shader
    //frag_tex_coords = tex_coord; //pour cube
    frag_tex_coords = position.xy;
}
