#!/usr/bin/env python3
import random
import OpenGL.GL as GL              # standard Python OpenGL wrapper
import glfw                         # lean window system wrapper for OpenGL
import numpy as np                  # all matrix manipulations & OpenGL args
from texture import Texture, Textured

from core import *
from transform import *




# -------------- Example textured plane class ---------------------------------
class TexturedPlane(Textured):
    """ Simple first textured object """
    def __init__(self, shader, tex_file1, tex_file2=None, indices = [], coords = [], tex = None, scaleSize = 30, **params):
        # prepare texture modes cycling variables for interactive toggling
        self.wraps = cycle([GL.GL_REPEAT, GL.GL_MIRRORED_REPEAT,
                            GL.GL_CLAMP_TO_BORDER, GL.GL_CLAMP_TO_EDGE])
        self.filters = cycle([(GL.GL_NEAREST, GL.GL_NEAREST),
                              (GL.GL_LINEAR, GL.GL_LINEAR),
                              (GL.GL_LINEAR, GL.GL_LINEAR_MIPMAP_LINEAR)])
        self.wrap, self.filter = next(self.wraps), next(self.filters)
        self.file = tex_file1
        self.file2 = tex_file2

        # setup plane mesh to be textured       
        scaleNbrTriangle = 1 # scale the number of triangle composing the ground
        # scaleSize = 30 # scale the size of each triangle composing the ground carefull as this also move the ground out of position
        
        scaled = scaleNbrTriangle * np.array(coords, np.float32) 
        tex_coord = scaleNbrTriangle * np.array(((0, 0), (1, 0), (1, 1), (0, 1)), dtype=np.float32)
         
        mesh = Mesh(shader, attributes=dict(position=scaled*scaleSize, tex_coord=tex_coord, normal= [[0,0,1] for i in range (4)]), index=indices)
    
        if tex is None:
            texture = Texture(tex_file1, self.wrap, *self.filter)
        else:
            texture = tex

        if tex_file2 is  None:
            tex_file2 = Texture(tex_file2, self.wrap, *self.filter)
        else:
            tex_file2 =  Texture("./ground/rocks.png", self.wrap, *self.filter)

        super().__init__(mesh, diffuse_map=texture, second_texture = tex_file2)


    def key_handler(self, key):
        # cycle through texture modes on keypress of F6 (wrap) or F7 (filtering)
        self.wrap = next(self.wraps) if key == glfw.KEY_F6 else self.wrap
        self.filter = next(self.filters) if key == glfw.KEY_F7 else self.filter
        if key in (glfw.KEY_F6, glfw.KEY_F7):
            texture = Texture(self.file, self.wrap, *self.filter)
            self.textures.update(diffuse_map=texture)



# -------------- Make a node out of an object --------------------------------
class ObjectLoader(Node):
    def __init__(self, shader, fileName, tex_file, light_dir = None): #tex_file = None
        super().__init__()
        if(light_dir == None):
            self.add(*load(fileName, shader,tex_file=tex_file))  
        else:
            self.add(*load(fileName, shader,tex_file=tex_file, light_dir= light_dir))  

# -------------- Generate object Procedurally --------------------------------
        
def generate_plant(shader, depth, max_depth=3, fileName = "./plant/cactus.obj"):
    """Generate a plant recursively."""
    if depth >= max_depth:
        return None  # Stop recursion if reached maximum depth

    # Create a node for the current part of the plant    
    branch = ObjectLoader(shader,fileName,"./plant/cactus.png")
    

    # Recursively generate child parts of the plant and attach them to the current part
    child_node = generate_plant(shader, depth + 1, max_depth, fileName= fileName)
    if child_node:
        branch.add(child_node)
        # Apply random rotation to the current part
        axis = (0, 1., 0.2)  # Rotation axis around the y-axis and tilt the branch
        angle = random.uniform(0, 360)  # Generate a random angle
        child_node.transform = rotate(axis, angle) @ translate(0,0.4,0) @ scale(0.9,1.3,0.9) #scale make the branch longer and tinner

    return branch


# -------------- main program and scene setup --------------------------------
def main():

    #vertex_shader_source = "texture.vert"
    """ create a window, add scene objects, then run rendering loop """
    viewer = Viewer(width=1280, height=720)
    viewer.trackball.distance = 36
    viewer.trackball.rotation = quaternion(-.5, 0, 0, 1)    
    viewer.trackball.pos2d  = vec(-6.0, -10)

    # ---Cube map----

    skyboxShader = Shader("./skybox/skybox.vert", "./skybox/skybox.frag")

    faces = [
        "./skybox/right.png",
        "./skybox/left.png",
        "./skybox/top.png",
        "./skybox/bottom.png",
        "./skybox/front.png",
        "./skybox/back.png"
    ]
    skybox = Skybox(skyboxShader,faces)
    viewer.add(skybox)

    #--------------- Make a tornado  --------------------
    
    shaderTornado = Shader("./tornado/tornado.vert", "./tornado/tornado.frag")

    TornadoNode = Node()
    TornadoNode.add(PointAnimation(shaderTornado,color = (0.8, 0.3, 0.0), center = (10,10,5))) 

    viewer.add(TornadoNode)

    #--------------- invoke Suzanne the god of light (is affected by light) --------------------
    
    suzanneShader = Shader("./suzanne/suzanne.vert", "./suzanne/suzanne.frag")

    suzanneNode = Node()
    light_dir = (0,0.5,0)

    suzanne = ObjectLoader(suzanneShader,"./suzanne/suzanne.obj", tex_file="./ground/burnt_sand.png",light_dir=light_dir)
    suzanneNode.add(suzanne) 

    suzanneNode.transform = translate(10,10,2.3) @ rotate((1,-.5,.5), 90) #move the ground by 1 unit up      
    viewer.add(suzanneNode)

    #------------Terrain------------------
    elevation = 2 #higher = less flat


    tableau_plane = [[[(i/10,j/10,random.uniform(1,elevation)/20), 10*i + j] for j in range(10)] for i in range(10)]
    base_coords = []   
    for i in range(10):
        for j in range(10):
            base_coords.append(tableau_plane[i][j][0])

    indiceGround = [2,1,0,2,0,3] #for a quad with 4 vertices
    indiceGround = np.array(indiceGround, np.uint32)
    
    shaderCactus = Shader("./plant/cactus.vert", "./plant/cactus.frag")
    shaderGround = Shader("./ground/ground.vert", "./ground/ground.frag")
    for i in range(9):
        for j in range(9):
            coords = [base_coords[10*i+j], base_coords[10*i+j+1], base_coords[10*(i+1)+j+1], base_coords[10*(i+1)+j]]
            
            scaled = 30
            #--------------this section will generate plant---------------
            for vertex in base_coords:
                x, y, z = vertex  # Extract coordinates
                generatePlant = random.uniform(0,1000)   
                plantDepth = random.uniform(2,6)   
                
                if(generatePlant>998):               
                   
                    plant_root = generate_plant(shaderCactus, depth=0, max_depth = plantDepth, fileName ="./plant/cactus.obj")
                    plant_root.transform = translate(x*scaled,y*scaled,z*scaled)  @ rotate((1,0,0), 90)#move the plant to the position of the point      
                    viewer.add(plant_root)
            #------------------------------------------------

            viewer.add(TexturedPlane(shaderGround, "./ground/burnt_sand.png",tex_file2= "./ground/rocks.png" , indices = indiceGround, coords = coords,scale2 = scaled))
           
 

    print("######################################################")
    print("######################################################")
    print("Control: ")
    print(" Rotate camera: left click drag")
    print(" Move camera: right click drag")
    print(" Zoom: scroll wheel")
    print(" Move the tornado: Arrow Key")
    print("######################################################")
    print("######################################################")
    viewer.run()


if __name__ == '__main__':
    main()                     # main function keeps variables locally scoped





###################################################



